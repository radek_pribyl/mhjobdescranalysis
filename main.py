from logging.config import dictConfig
import json
import os
from threads.manager import MHJobDescriptionManager
from dto.managerconfig import ThreadsManagerConfig
from dto.producerconfig import ProducerConfig
from db.db import configure_db_engine
from analysis import OutputAnalysis


def configure_logging(fname):
    path = os.path.abspath(fname)
    if os.path.exists(path):
        with open(path, 'rt') as lfile:
            json_config = json.load(lfile)
        dictConfig(json_config)


def configure_system():
    configure_logging('logging.json')
    configure_db_engine('MHouse')


def main():
    configure_system()
    producer_config = ProducerConfig(1000, 100, 1)
    man_config = ThreadsManagerConfig(
        producer_config, 4, 'vysledek.csv', 'statistics.txt', 'idoutput.csv')
    manager = MHJobDescriptionManager(man_config)
    manager.start_processing()
    analysis = OutputAnalysis('vysledek.csv', 'analysis.csv')
    analysis.run_analysis()

if __name__ == '__main__':
    main()
