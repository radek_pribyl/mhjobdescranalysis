from db.db import configure_db_engine, get_number_of_us_jobs, get_us_jobdetailids, get_jobbodies_with_http
from main import configure_logging

configure_db_engine('MHouse', True)

def test_dbcount():
    fdate = '3'
    result = get_number_of_us_jobs(fdate)
    print(result)

def test_get_us_jobs():
    fdate = '3'
    limit = 100
    lastid = 0
    result = get_us_jobdetailids(fdate, limit, lastid)
    print("Size: {0}".format(len(result)))
    print("Max: {0}".format(max(result)))

def test_get_jobbodies_with_http():
    jobids = [127539895, 127539900, 127539903, 127539905]
    result = get_jobbodies_with_http(jobids)
    for row in result:
        print(row)

configure_logging('logging.json')
#test_get_us_jobs()
#test_dbcount()
test_get_jobbodies_with_http()
