class ProducerConfig(object):

    def __init__(self, numperbatch, numpertask, numdays):
        self._numperbatch = numperbatch
        self._numpertask = numpertask
        self._numdays = numdays

    @property
    def numperbatch(self):
        return self._numperbatch

    @property
    def numpertask(self):
        return self._numpertask

    @property
    def numdays(self):
        return self._numdays
