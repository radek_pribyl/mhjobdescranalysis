class ThreadsManagerConfig(object):

    def __init__(self, producerconfig, numofconsumers, ofile, sfile, idfile):
        self._producerconfig = producerconfig
        self._numofconsumers = numofconsumers
        self._outputfile = ofile
        self._idoutputfile = idfile
        self._statisticfile = sfile

    @property
    def producerconfig(self):
        return self._producerconfig

    @property
    def numofconsumers(self):
        return self._numofconsumers

    @property
    def outputfile(self):
        return self._outputfile

    @property
    def statisticsfile(self):
        return self._statisticfile

    @property
    def idoutputfile(self):
        return self._idoutputfile