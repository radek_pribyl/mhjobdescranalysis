import threading
import logging
from threads.sentinel import _Sentinel

class QueueConsumer(threading.Thread):

    def __init__(self, queue, name='QueueConsumer'):
        self._queue = queue
        self.running = False
        self.logger = logging.getLogger('%s.%s' % (self.__class__.__module__,
                                                   self.__class__.__name__))
        threading.Thread.__init__(self, name=name)

    def run(self):
        self.logger.info("Thread %s starting", self.getName())
        self.running = True
        self._run_before_loop()
        self._run_main_loop()
        self._run_after_loop()
        self.logger.info("Thread %s exiting", self.getName())

    def _run_before_loop(self):
        pass

    def _run_main_loop(self):
        while self.running:
            task = self._queue.get()
            if isinstance(task, _Sentinel):
                self.logger.debug("Sentinel value found in queue")
                self.running = False
                self._queue.task_done()
                break
            self._try_process_task(task)

    def _run_after_loop(self):
        pass

    def _try_process_task(self, task):
        try:
            self._process_task(task)
            self._queue.task_done()
        except Exception:
            self.logger.exception("Exception in thread %s", self.getName())
            self.running = False

    def _process_task(self, task):
        raise NotImplementedError

    def stop(self):
        self.running = False
