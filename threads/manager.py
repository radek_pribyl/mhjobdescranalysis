import logging
import queue
from threads.producer import ProducerThread
from threads.regex import RegexThread
from threads.printer import PrinterThread
from threads.statistic import StatisticThread
from threads.sentinel import _Sentinel
from threads.idprinter import IdPrinterThread


class MHJobDescriptionManager(object):

    def __init__(self, manconfig):
        self._config = manconfig
        self._inqueue = queue.Queue()
        self._resqueue = queue.Queue()
        self._jobdetailqueue = queue.Queue()
        self._squeue = queue.Queue()
        self._producer = None
        self._consumers = []
        self._printer = None
        self._idprinter = None
        self._statistics = None
        self.logger = logging.getLogger('%s.%s' % (self.__class__.__module__,
                                                   self.__class__.__name__))

    def start_processing(self):
        self._start_producer()
        self._start_consumers()
        self._start_printers()
        self._start_statistics()
        self._notify_consumers_to_finish()
        self._notify_out_threads_to_finish()
        self.logger.debug("Size of inqueue: %s", self._inqueue.qsize())
        self.logger.debug("Size of resqueue: %s", self._resqueue.qsize())
        self.logger.debug("Size of squeue: %s", self._squeue.qsize())
        self.logger.debug("Size of jobidqueue: %s", self._jobdetailqueue.qsize())
        self.logger.info("MHJobDescriptionManager exiting")

    def _start_producer(self):
        self.logger.info('Starting producer thread')
        self._producer = ProducerThread(
            self._config.producerconfig, self._inqueue, self._squeue)
        self._producer.start()

    def _start_consumers(self):
        for idx in range(self._config.numofconsumers):
            self.logger.info('Starting Consumer #: %s', idx)
            consumer = RegexThread(
                self._inqueue, self._resqueue, self._squeue, self._jobdetailqueue, idx)
            consumer.start()
            self._consumers.append(consumer)

    def _start_printers(self):
        self.logger.info('Starting printer thread')
        self._printer = PrinterThread(self._resqueue, self._config.outputfile)
        self._printer.start()
        self._idprinter = IdPrinterThread(
            self._jobdetailqueue, self._config.idoutputfile)
        self._idprinter.start()

    def _start_statistics(self):
        self.logger.info('Starting statistic thread')
        self._statistics = StatisticThread(
            self._squeue, self._config.statisticsfile, self._config.producerconfig.numdays)
        self._statistics.start()

    def _notify_consumers_to_finish(self):
        self._producer.join()
        self.logger.info('Notifying consumers - sending Sentinel')
        for _ in range(self._config.numofconsumers):
            self._inqueue.put(_Sentinel())

    def _notify_out_threads_to_finish(self):
        for consumer in self._consumers:
            consumer.join()
        self.logger.info('Notifying printer thread - sending Sentinel')
        self._resqueue.put(_Sentinel())
        self.logger.info('Notifying idprinter thread - sending Sentinel')
        self._jobdetailqueue.put(_Sentinel())
        self.logger.info('Notifying statistics thread - sending Sentinel')
        self._squeue.put(_Sentinel())
        self._printer.join()
        self._idprinter.join()
        self._statistics.join()
