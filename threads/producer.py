import threading
import logging
from db.db import get_us_jobdetailids


class ProducerThread(threading.Thread):

    def __init__(self, config, inqueue, squeue):
        self._config = config
        self._inqueue = inqueue
        self._squeue = squeue
        self._processing = True
        self._jobdetailids = None
        self.logger = logging.getLogger('%s.%s' % (self.__class__.__module__,
                                                   self.__class__.__name__))
        name = "ProducerThread"
        threading.Thread.__init__(self, name=name)

    def run(self):
        last_id = 0
        while self._processing:
            self._jobdetailids = get_us_jobdetailids(
                self._config.numdays, self._config.numperbatch, last_id)
            if self._jobdetailids:
                last_id = max(self._jobdetailids)
                self.logger.info('Increasing last_id to %s', last_id)
                self._create_tasks()
            else:
                self.logger.debug('No result. Finishing')
                self._processing = False

    def _create_tasks(self):
        while self._jobdetailids:
            task = []
            num_of_loops = min(self._config.numpertask,
                               len(self._jobdetailids))
            for _ in range(num_of_loops):
                jobdetailid = self._jobdetailids.pop()
                task.append(jobdetailid)
            self.logger.info('Adding task with %s entries', len(task))
            self._inqueue.put(task)
            self._send_statistics(len(task))

    def _send_statistics(self, numjobs):
        entry = ('num of jobdetailids', numjobs)
        self._squeue.put(entry)
