from db.db import get_jobbodies_with_http
from threads.queueconsumer import QueueConsumer
from regex.httpresource import HttpResourceRegex


class RegexThread(QueueConsumer):

    def __init__(self, queue, resqueue, squeue, idqueue=None, num=0):
        self._resqueue = resqueue
        self._squeue = squeue
        self._idoutputqueue = idqueue
        self._regex = HttpResourceRegex()
        name = "RegexThread-{0}".format(num)
        QueueConsumer.__init__(self, queue, name)

    def _process_task(self, task):
        self.logger.info("Calling db with %s ids", len(task))
        jobbodies = get_jobbodies_with_http(task)
        self.logger.info("Received %s bodies with http", len(jobbodies))
        self._send_http_statistics(len(jobbodies))
        self._scan_jobbodies(jobbodies)

    def _scan_jobbodies(self, jobbodies):
        matchedcounter = 0
        for jobdetailid, jobbody in jobbodies:
            matched = self._regex.findall(jobbody)
            if matched:
                entry = (jobdetailid, matched)
                self.logger.debug("Found %s", entry)
                self._resqueue.put(entry)
                matchedcounter += 1
            else:
                if self._idoutputqueue:
                    self._idoutputqueue.put(jobdetailid)
        self._send_matched_statistics(matchedcounter)

    def _send_http_statistics(self, numofjobs):
        entry = ('num of jobs with http', numofjobs)
        self._squeue.put(entry)

    def _send_matched_statistics(self, numofjobs):
        entry = ('num of jobs matched regex', numofjobs)
        self._squeue.put(entry)
