from collections import Counter
from db.db import get_number_of_us_jobs
from threads.queueconsumer import QueueConsumer


class StatisticThread(QueueConsumer):

    def __init__(self, queue, sfile, numdays):
        self._sfile = sfile
        self._numdays = numdays
        self._counter = Counter()
        name = "StatisticThread"
        QueueConsumer.__init__(self, queue, name)

    def _run_before_loop(self):
        num_of_all_jobs = get_number_of_us_jobs(self._numdays)
        self._counter['all jobs from db'] = num_of_all_jobs
        self.logger.info('Number of expected jobs for last %s days is %s',
                         self._numdays, num_of_all_jobs)

    def _run_after_loop(self):
        self.logger.info('Saving statistics')
        with open(self._sfile, 'wt', encoding='utf-8') as ofile:
            for stat, value in self._counter.items():
                line = "Total number for '{0}': {1}\n".format(stat, value)
                ofile.write(line)

    def _process_task(self, task):
        self.logger.debug('Processing task %s', task)
        try:
            stat, value = task
            self._counter[stat] += value
        except (TypeError, ValueError):
            self.logger.exception('Exception when processing task')

    def get_actual_value(self, stat):
        return self._counter[stat]
