from threads.queueconsumer import QueueConsumer


class PrinterThread(QueueConsumer):

    def __init__(self, queue, fname):
        self._fname = fname
        self._ofile = None
        name = 'PrinterThread'
        QueueConsumer.__init__(self, queue, name)

    def _run_before_loop(self):
        self.logger.info('Opening %s for write', self._fname)
        self._ofile = open(self._fname, 'wt', encoding='utf-8')
        header = 'JobDetailId,Matches\n'
        self._ofile.write(header)

    def _run_after_loop(self):
        if self._ofile:
            self.logger.info('Closing %s file', self._fname)
            self._ofile.close()

    def _process_task(self, task):
        line = str(task[0]) + ',' + task[1] + '\n'
        self._ofile.write(line)
