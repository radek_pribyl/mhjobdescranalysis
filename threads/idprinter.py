from threads.queueconsumer import QueueConsumer


class IdPrinterThread(QueueConsumer):

    def __init__(self, queue, fname):
        self._fname = fname
        self._ofile = None
        name = 'IdPrinterThread'
        QueueConsumer.__init__(self, queue, name)

    def _run_before_loop(self):
        self.logger.info('Opening %s for write', self._fname)
        self._ofile = open(self._fname, 'wt', encoding='utf-8')

    def _run_after_loop(self):
        if self._ofile:
            self.logger.info('Closing %s file', self._fname)
            self._ofile.close()

    def _process_task(self, task):
        self.logger.debug('Processing task %s', task)
        line = str(task) + ','
        self._ofile.write(line)
