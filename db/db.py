import logging
import time
import inspect
from sqlalchemy import create_engine, Integer, String
from sqlalchemy.sql import text

engine = None


def configure_db_engine(dsname, debug=False):
    global engine
    conn_str = "mssql+pyodbc://{0}".format(dsname)
    engine = create_engine(conn_str, echo=debug)


def connectivity_test():
    query = text(
        "SELECT JobBody FROM JobDetailBody (nolock) WHERE JobDetailID=:detailid")
    conn = engine.connect()
    return conn.execute(query, {"detailid": 103037544})


def get_number_of_us_jobs(daysback):
    logging.info('Executing get_number_of_us_jobs with %s',
                 daysback)
    query = """
    select count(*) from JobDetailBody (nolock) 
        where JobDetailID in 
            (SELECT distinct JP.JobDetailID
            FROM JobPosition JP (nolock)
	            join JobPositionAd JPA (nolock) 
                on JP.JobPositionID = JPA.JobPositionID
	            join JobPositionPhysicalAddress JPPA (nolock) 
                on JP.JobPositionPhysicalAddressID = JPPA.JobPositionPhysicalAddressID
            WHERE JPPA.CountryID = 164
	        AND JPA.JobBoardID = 1
	        AND JPA.FirstDatePosted > DATEADD(day, -{0}, GETDATE()))
    """
    query = query.format(daysback)
    query_result = _execute_query(query)
    return query_result.scalar()


def _execute_query(query_text):
    logging.debug("Executing: %s", query_text)
    start_time = time.time()
    conn = engine.connect()
    query_result = conn.execute(query_text)
    end_time = time.time()
    logging.info('The execution of %s took %s seconds', inspect.stack()[1][3],
                 end_time - start_time)
    return query_result


def _return_result_as_set(query_result):
    result = set()
    for row in query_result:
        result.add(row[0])
    return result


def get_us_jobdetailids(daysback, limit, lastid):
    logging.info('Executing get_us_jobdetailids with %s, %s, %s',
                 daysback, limit, lastid)
    query = """
SELECT distinct TOP {0} JP.JobDetailID
FROM JobPosition JP (nolock)
	join JobPositionAd JPA (nolock)
	on JP.JobPositionID = JPA.JobPositionID
	join JobPositionPhysicalAddress JPPA (nolock)
	on JP.JobPositionPhysicalAddressID = JPPA.JobPositionPhysicalAddressID
WHERE JPPA.CountryID = 164
	AND JPA.JobBoardID = 1
	AND JP.CreatedDate > DATEADD(day, -{1}, GETDATE())
	AND JP.JobDetailID > :lastid
ORDER BY JP.JobDetailID asc
"""
    query_txt = query.format(limit, daysback)
    query = text(query_txt).bindparams(
        lastid=lastid).columns(JobDetailID=Integer)
    query_result = _execute_query(query)
    return _return_result_as_set(query_result)


def _list_as_string(lids):
    result = ''
    for lid in lids:
        result += '{0},'.format(str(lid))
    return result[:-1]


def get_jobbodies_with_http(jobdetailids):
    logging.info('get_jobbodies_with_http %s',
                 jobdetailids)
    query_txt = """
SELECT JobDetailID, JobBody
FROM JobDetailBody (nolock)
WHERE JobDetailId IN ({0})
    AND JobBody like '%http://%'
"""
    query_txt = query_txt.format(_list_as_string(jobdetailids))
    query = text(query_txt).columns(
        JobDetailID=Integer, JobBody=String)
    query_result = _execute_query(query)
    return query_result.fetchall()


def run_analysis(jobdetailids):
    query_txt = """
SELECT C.CompanyName, C.CompanyXCode, JP.JobTemplateID, 
	COUNT(JP.JobTemplateID) AS 'NumOfJobs'
FROM JobPosition JP (nolock)
	join Recruiter R (nolock)
	on JP.UserID = R.UserID
	join Company C (nolock)
	on R.CompanyID = C.CompanyID
WHERE JP.JobDetailID IN ({0})
GROUP BY C.CompanyName, C.CompanyXCode, JP.JobTemplateID
"""
    query_txt = query_txt.format(_list_as_string(jobdetailids))
    query = text(query_txt).columns(
        CompanyName=String, CompanyXCode=String, JobTemplateID=Integer, NumOfJobs=Integer)
    query_result = _execute_query(query)
    return query_result.fetchall()
