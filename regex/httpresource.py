import re

class HttpResourceRegex(object):
    pattern = r"http:\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?\.(gif|png|jpeg|bmp|jpg|svg|tiff|js|css|ico|swf|dtd|htm|html)"

    def __init__(self):
        self._cregex = re.compile(HttpResourceRegex.pattern, re.IGNORECASE)

    def findfirst(self, text):
        matched = self._cregex.search(text)
        result = ""
        if matched:
            result = matched.group(0)
        return result

    def findall(self, text, delim='|'):
        matches = self._cregex.finditer(text)
        entries = (match.group(0) for match in matches)
        return delim.join(entries)
