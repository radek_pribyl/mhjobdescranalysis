from db.db import run_analysis


class OutputAnalysis(object):

    def __init__(self, infile, outfile):
        self._infile = infile
        self._outfile = outfile
        self._jobdetailids = []

    def run_analysis(self):
        self._read_jobdetailids()
        self._process_analysis()

    def _read_jobdetailids(self):
        with open(self._infile, 'r') as infile:
            _ = infile.readline()
            for line in infile.readlines():
                sline = line.split(',')
                self._jobdetailids.append(int(sline[0]))

    def _process_analysis(self):
        analysis = run_analysis(self._jobdetailids)
        self._save_analysis(analysis)

    def _save_analysis(self, analysis):
        with open(self._outfile, 'wt', encoding='utf-8') as ofile:
            header = 'CompanyName|CompanyXCode|JobTemplateID|NumOfJobs\n'
            ofile.write(header)
            for record in analysis:
                strrecord = (str(field).strip() for field in record)
                line = '|'.join(strrecord)
                line = line + '\n'
                ofile.write(line)
